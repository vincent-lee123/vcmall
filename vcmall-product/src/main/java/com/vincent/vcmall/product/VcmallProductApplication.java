package com.vincent.vcmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VcmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(VcmallProductApplication.class, args);
    }

}
